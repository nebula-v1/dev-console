﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Nebula.DevConsole
{
    internal class Test : MonoBehaviour
    {
        private void Start()
        {
            var activeScene = SceneManager.GetActiveScene();
            Scene t = SceneManager.CreateScene("Test");
            
            SceneManager.SetActiveScene(t);

            GameObject go = (GameObject)AssetDatabase.LoadAssetAtPath("Assets/Nebula.DevConsole/RuntimeAssets/Test.prefab", typeof(GameObject));
            Instantiate(go);

            SceneManager.SetActiveScene(activeScene);
        }

        /*
        [InitializeOnLoadMethod]
        private static void T()
        {
            if (!Application.isPlaying) return;
            
            List<EditorBuildSettingsScene> editorBuildSettingsScenes =
                new List<EditorBuildSettingsScene>(EditorBuildSettings.scenes);

            Debug.Log(editorBuildSettingsScenes[0].path);

            var t = new EditorBuildSettingsScene("Assets/Nebula.DevConsole/RuntimeAssets/Test.unity", true);

            editorBuildSettingsScenes.Add(t);

            EditorBuildSettings.scenes = editorBuildSettingsScenes.ToArray();

            Application.quitting += OnApplicationQuit;
        }*/
    }
}